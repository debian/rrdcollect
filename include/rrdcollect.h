/* rrdcollect.h - header file for RRDcollect. */
/*
 *  RRDcollect  --  Round-Robin Database Collecting Daemon.
 *  Copyright (C) 2002, 2003  Dawid Kuroczko <qnex@knm.org.pl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef HAVE_RRDCOLLECT_H
#define HAVE_RRDCOLLECT_H

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

/* default includes */
#include <stdio.h>
#if HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif
#if HAVE_SYS_STAT_H
# include <sys/stat.h>
#endif
#if STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# if HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif
#if HAVE_STRING_H
# if !STDC_HEADERS && HAVE_MEMORY_H
#  include <memory.h>
# endif
# include <string.h>
#endif
#if HAVE_STRINGS_H
# include <strings.h>
#endif
#if HAVE_INTTYPES_H
# include <inttypes.h>
#else
# if HAVE_STDINT_H
#  include <stdint.h>
# endif
#endif
#if HAVE_UNISTD_H
# include <unistd.h>
#endif

/* time(2) */
#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

/* other includes */
#include <ctype.h>
#include <stdarg.h>
#include <errno.h>

#ifdef HAVE_GETOPT_LONG
# include <getopt.h>
#else
# include "getopt.h"
#endif /* HAVE_GETOPT_LONG */

/* libraries */
#ifdef HAVE_LIBRRD
# ifdef HAVE_RRD_RRD_H
#  include <rrd/rrd.h>
# else /* HAVE_RRD_RRD_H */
#  include <rrd.h>
# endif /* HAVE_RRD_RRD_H */
#endif /* HAVE_LIBRRD */

#ifdef HAVE_LIBPCRE
# ifdef HAVE_PCRE_PCRE_H
#  include <pcre/pcre.h>
# else /* HAVE_PCRE_PCRE_H */
#  include <pcre.h>
# endif /* HAVE_PCRE_PCRE_H */
#endif /* HAVE_LIBPCRE */

#ifdef HAVE_LIBPCAP
# ifdef HAVE_PCAP_PCAP_H
#  include <pcap/pcap.h>
# else /* HAVE_PCAP_PCAP_H */
#  include <pcap.h>
# endif /* HAVE_PCAP_PCAP_H */
#endif /* HAVE_LIBPCAP */

/* config file */
#ifndef RRDCOLLECT_CONF
# ifdef SYSCONFDIR
#  define RRDCOLLECT_CONF SYSCONFDIR "/rrdcollect.conf"
# else /* SYSCONFDIR */
#  define RRDCOLLECT_CONF "/etc/rrdcollect.conf"
# endif /* SYSCONFDIR */
#endif /* RRDCOLLECT_CONF */

/* error/debug output */
#include <syslog.h>
#define OUTPUT_STDERR 0
#define OUTPUT_SYSLOG 1

/* 
 * structs
 */

struct counter_t {
	const char *name;
	char *value;
};

struct rrd_t {
	const char *rrdfile;
	int count;
	char *rrdtemplate;
	struct counter_t **counter;
};

struct uri_t {
	const char *domain;
	/* TODO: 
	 * enum domain_t {
		RRDCOLLECT_FILE,
		RRDCOLLECT_EXEC,
		RRDCOLLECT_SNMP
	}
	*/
	const char *host;
	const char *path;
};

enum match_type_t {
	MATCH_PATTERN,
#ifdef HAVE_LIBPCRE
	MATCH_REGEX,
#endif /* HAVE_LIBPCRE */
	MATCH_PATSUBST
};

struct match_t {
	int line;			/* to which line it applies */
	int done;			/* done already? */

	int count;			/* how many results does it make */
	struct counter_t **counter;	/* and where to store them */
	
	enum match_type_t type;		/* what is it exactly? */

/* FIXME: Name the union or something, instead of this HAVE_ANONYMOUS_UNION */
#ifdef HAVE_ANONYMOUS_UNION
	union {
#endif /* HAVE_ANONYMOUS_UNION */
		const char *pattern;	/* a scanf(3)-style pattern */
#ifdef HAVE_LIBPCRE
		struct {
			pcre *code;
			pcre_extra *extra;
			int *ovector;
		} regex;		/* perl compatible regex */
#endif /* HAVE_LIBPCRE */
#ifdef HAVE_ANONYMOUS_UNION
	};
#endif /* HAVE_ANONYMOUS_UNION */
};

struct test_t {
	struct uri_t *uri;
	int count;
	struct match_t **match;
};

/*
 * externs
 */

/* scan.c */
extern int scan(const char *buf, const char *fmt, struct counter_t **counter);
extern int scan_count(const char *fmt);
extern int regscan(const char *bp, const struct match_t *match);

/* counters.c */
extern struct counter_t *add_rrd_counter(const char *rrdfile, const char *name);
extern struct test_t *add_test_uri(const struct uri_t *uri);
extern void clear_counters(void);
extern void perform_tests(void);
extern void print_results(void);

/* log.c */
extern void start_log(void);
extern void stop_log(void);
extern void send_log(int prio, char *fmt, ...);

/* checkpid.c */
extern int is_another_copy(void);
extern void write_pidfile(void);
extern void release_pidfile(void);

/* parse.c */
extern char buf[8192];

extern int parse_conf(const char *conf_file);

/* rrdcollect.c */
extern int (*make_update)(int argc, char **argv);
extern int set_config_variable(const char *name, const char *value);

/* parser.y */

/* scanner.l */
extern int yylex(void);


#endif /* HAVE_RRDCOLLECT_H */
