/* log.c - syslog support, output errors */
/*
 *  RRDcollect  --  Round-Robin Database Collecting Daemon.
 *  Copyright (C) 2003  Artur R. Czechowski <arturcz@hell.pl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "rrdcollect.h"
#include <syslog.h>
#include <stdio.h>
#include <stdarg.h>

extern int debugoutput;
extern int debuglevel;

int logger_subsystem_initialized=1;

void start_log(void) {
	switch(debugoutput) {
		case OUTPUT_SYSLOG:
			openlog("rrdcollect",LOG_CONS|LOG_PID,LOG_DAEMON);
			break;
		case OUTPUT_STDERR:
			break;
		default: /* Murphy's Law */
			fprintf(stderr,"start_log: Fatal error in "__FILE__":%i. Please send a bugreport.\n",__LINE__);
			exit(1);
	}
	logger_subsystem_initialized=1;
}

void stop_log(void) {
	switch(debugoutput) {
		case OUTPUT_SYSLOG:
			closelog();
			break;
		case OUTPUT_STDERR:
			break;
		default: /* Murphy's Law */
			fprintf(stderr,"stop_log: Fatal error in "__FILE__":%i. Please send a bugreport.\n",__LINE__);
			exit(1);
	}
	logger_subsystem_initialized=0;
}

void send_log(int prio, char *fmt, ...) {
	va_list ap;
	va_start(ap,fmt);
	if(!logger_subsystem_initialized) {
		vfprintf(stderr,fmt,ap);
		fprintf(stderr,"send_log: Fatal error. Logger subsystem uninitialized. Please send a bugreport.\n");
		exit(1);
	}
	switch(debugoutput) {
		case OUTPUT_SYSLOG:
			if(prio<=debuglevel)
				vsyslog(prio,fmt,ap);
			break;
		case OUTPUT_STDERR:
			if(prio<=debuglevel)
				vfprintf(stderr,fmt,ap);
			break;
		default: /* Murphy's Law */
			fprintf(stderr,"Fatal error in "__FILE__":%i. Please end a bugreport.\n",__LINE__);
			exit(1);
	}
	va_end(ap);
}
