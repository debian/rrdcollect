/* rrdcollect.c - main(), command line, forking, main loop. */
/*
 *  RRDcollect  --  Round-Robin Database Collecting Daemon.
 *  Copyright (C) 2002, 2003  Dawid Kuroczko <qnex@knm.org.pl>
 *  Copyright (C) 2003-2009  Artur R. Czechowski <arturcz@hell.pl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "rrdcollect.h"

#include <fcntl.h>

#include <getopt.h>

#include <signal.h>

#include <sys/time.h>

#include <stdio.h>
#include <string.h>

/* command line options */
const char *config_file = RRDCOLLECT_CONF;
enum working_mode {
#ifdef HAVE_LIBRRD
	LIBRRD_MODE,
#endif /* HAVE_LIBRRD */
	PIPE_MODE,
	FILE_MODE
} mode;
const char *output_pipe = RRDTOOL;
const char *output_file = NULL;
FILE *rrdtool = NULL;

int verbose = 0;
int no_fork = 0;

/* config options */
const char *workdir = NULL;
int step = 60;
int debugoutput = OUTPUT_SYSLOG;
int debuglevel = LOG_NOTICE;  /* from syslog.h */

/* signals */
volatile int action_request = 0;

void do_nothing(__attribute__ ((unused)) int signum) { }
void do_action(int signum) { action_request = signum; }
void do_abort(__attribute__ ((unused)) int signum) {
	send_log(LOG_ERR, "Segmentation fault (core dumped), but The Memory Remains.\n\n");
	fprintf(stderr, "\nSegmentation fault (core dumped), but The Memory Remains.\n\n");
	fflush(stderr);
	abort();
}

/* data delivery */
int print_update(int argc, char **argv)
{
	int i;
	for (i = 0; i < argc && argv[i]; i++) {
		fprintf(rrdtool, (i) ? " %s" : "%s", argv[i]);
		if (verbose)
			send_log(LOG_DEBUG, (i) ? " %s" : "%s", argv[i]);
	}
	putc('\n', rrdtool);

	return 0;
}

#ifdef HAVE_LIBRRD
#define MAX_LOG_BUF_LEN 2048
int rrdlib_update(int argc, char **argv)
{
	char logbuffer[MAX_LOG_BUF_LEN];
	int i;
	int stoplog;

	optind = opterr = 0;
	
	logbuffer[0]='\0';
	stoplog=0;
	for(i=0;(i<argc)&&(!stoplog);i++) {
		if(i>0) {
			if(strlen(logbuffer)+3>=MAX_LOG_BUF_LEN) {
				send_log(LOG_DEBUG,"Report an error: Increase MAX_LOG_BUF_LEN\n");
				stoplog=1;
			} else {
				strcat(logbuffer," : ");
			}
		}
		if(stoplog)
			break;

		if(strlen(logbuffer)+strlen(argv[i])>=MAX_LOG_BUF_LEN) {
			send_log(LOG_DEBUG,"Report an error: Increase MAX_LOG_BUF_LEN\n");
			stoplog=1;
		} else {
			strcat(logbuffer,argv[i]);
		}
	}
	if(stoplog)
		send_log(LOG_DEBUG,"Incomplete update in log");
	send_log(LOG_DEBUG,"rrdlib_update(%i,'%s')",argc,logbuffer);

	rrd_update(argc, argv);

	if (rrd_test_error()) {
		send_log(LOG_ERR, "rrd_update: %s\n", rrd_get_error());
		/* FIXME-ac error handling */
		rrd_clear_error();
		
		return 1;
	}

	return 0;
}
#endif /* HAVE_LIBRRD */

int (*make_update)(int argc, char **argv);


int set_config_variable(const char *name, const char *value)
{
#define strpar(a) {#a,a}
	struct {char *f; int v;} lls[]={
		strpar(LOG_EMERG),
		strpar(LOG_ALERT),
		strpar(LOG_CRIT),
		strpar(LOG_ERR),
		strpar(LOG_WARNING),
		strpar(LOG_NOTICE),
		strpar(LOG_INFO),
		strpar(LOG_DEBUG),
		{NULL,-1}
	};
#undef strpar
	switch (*name) {
		case 'd':
			if (!strcmp(name, "directory")) {
				workdir = strdup(value);
				return 0;
			}
			break;
		case 'l':
			if(!strcmp(name, "loglevel")) {
				int i=0;
				debuglevel = LOG_WARNING;
				while(lls[i].v!=-1) {
					if(!strcmp(value,lls[i].f)) {
						debuglevel = lls[i].v;
						break;
					}
					++i;
				}
				send_log(LOG_NOTICE,"Log level: %s\n",lls[i].f);
				return 0;
			}
		case 's':
			if (!strcmp(name, "step")) {
				step = atoi(value);
				if (!step) {
					send_log(LOG_ERR, "Invalid step value: %s (%d)\n", value, step);
					exit(1);
				}
				return 0;
			}
			break;
		default:
			return 1;	
	}
	return 2;
}

/* some sugar */
void usage(int mode)
{
	/* usage */
	if (mode == -2) {
		printf("Usage: rrdcollect [-v] [--config=PATH] [--help] [--usage]\n");
		exit(0);
	}
	/* version */
	if (mode == -1) {
		printf(PACKAGE_STRING
#if HAVE_LIBRRD || HAVE_LIBPCRE || !HAVE_GETOPT_LONG
				" (compiled with "
# ifdef HAVE_LIBRRD
				"librrd"
#  if HAVE_LIBPCRE || !HAVE_GETOPT_LONG
				" and "
#  endif /* HAVE_LIBPCRE || !HAVE_GETOPT_LONG */
# endif /* HAVE_LIBRRD */
# ifdef HAVE_LIBPCRE
				"libpcre"
#  ifndef HAVE_GETOPT_LONG
				" and "
#  endif /* !HAVE_GETOPT_LONG */
# endif /* HAVE_LIBPCRE */
# ifndef HAVE_GETOPT_LONG
				"built-in GNU getopt_long(3)"
# endif /* !HAVE_GETOPT_LONG */
				")"
#endif /* HAVE_LIBRRD || HAVE_LIBPCRE || !HAVE_GETOPT_LONG */
			"\n");
		printf("Copyright (C) 2002  Dawid Kuroczko <qnex@knm.org.pl>\n\n");

		printf("This program is distributed in the hope that it will be useful,\n");
		printf("but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
		printf("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
		printf("GNU General Public License for more details.\n\n");
		exit(0);
	}

	printf(PACKAGE_STRING "  --  Round-Robin Database Collecting Daemon.\n");
	printf("Usage: rrdcollect [OPTION]...\n");

	/* error */
	if (mode > 0) {
		printf("\nTry `rrdcollect --help' for more options.\n");
		exit(1);
	}
	printf("\nGeneral options:\n");
	printf("  -c,  --config=FILE       specify alternative config file.\n");
	printf("  -n,  --no-fork           do not even think about forking.\n");
	printf("  -v,  --verbose           increase verbosity, implies -n.\n");

	printf("\nRRD update options:\n");
	printf("  -o,  --output=FILE       writes update commands to FILE.\n");
	printf("  -p,  --pipe[=RRDTOOL]    opens pipe to RRDTOOL and sends it commands.\n");
	printf("\nIf no RRD update option is given, " PACKAGE_NAME
#ifdef HAVE_LIBRRD
			" uses internal librrd routines.\n");
#else
			" opens pipe to " RRDTOOL " program.\n");
#endif /* HAVE_LIBRRD */
	
	/* help */
	printf("\nHelp options:\n");
	printf("  -V,  --version           display the version of " PACKAGE_NAME " and exit.\n");
	printf("  -h,  --help              print this help.\n");
	printf("       --usage             terse usage information.\n");

	printf("\nMail bug reports and suggestions to <" PACKAGE_BUGREPORT ">\n");
	return exit(0);
};

int main(int argc, char **argv)
{
	int c;

	struct sigaction action;
	struct itimerval period;
	int loop;

	/* for safe exec:// */
	setenv("IFS"," \t\n",1);

	/* command line options */

#ifdef HAVE_LIBRRD
	mode = LIBRRD_MODE;
#else
	mode = PIPE_MODE;
#endif /* HAVE_LIBRRD */
	
	start_log();

	while (1) {
		int option_index = 0;
	
		static struct option long_options[] = {
			{"verbose", 0, 0, 'v'},
			{"no-fork", 0, 0, 'n'},
			{"config", 1, 0, 'c'},
			
			{"output", 1, 0, 'o'},
			{"pipe", 2, 0, 'p'},

			{"version", 0, 0, 'V'},
			{"help", 0, 0, 'h'},
			{"usage", 0, 0, 'U'},
			{0, 0, 0, 0}
		};

		c = getopt_long(argc, argv, "vnc:o:pVh",
				long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
			case 'c':
				config_file = optarg;
				break;
			case 'v':
				verbose++;
			case 'n':
				no_fork = 1;
				break;

			case 'o':
				mode = FILE_MODE;
				output_file = optarg;
				break;
			case 'p':
				mode = PIPE_MODE;
				if (optarg) {
					output_pipe = optarg;
				}
				break;
				
			case 'h':
				usage(0);
				break;
			case 'V':
				usage(-1);
				break;
			case 'U':	/* --usage */
				usage(-2);
				break;
		}
	}

	if (optind < argc) {
		usage(1);
		return 1;
	}

	if (parse_conf(config_file)) {
		send_log(LOG_ERR, "Cannot parse config file %s: %s\n", config_file, strerror(errno));
		return 2;
	}

	if (!workdir) {
		send_log(LOG_ERR, "You have to give working directory in config file\n");
		send_log(LOG_ERR, "If you're desperate, you can use something like this:\n");
		send_log(LOG_ERR, "    # current directory:\n");
		send_log(LOG_ERR, "    directory = .\n");
		return 2;
	}

	send_log(LOG_NOTICE,"%s started",argv[0]);

	if(is_another_copy()) {
		send_log(LOG_ERR,"Another copy of program is running, exiting\n");
		exit(1);
	}
	
	/* Open Sesame... */
#ifdef HAVE_LIBRRD
	if (mode == LIBRRD_MODE) {
		make_update = &rrdlib_update;
		send_log(LOG_INFO,"Update method: rrdlib");
	} else
#endif /* HAVE_LIBRRD */
	{
		make_update = &print_update;
		send_log(LOG_INFO,"Update method: print");
		if (mode == FILE_MODE) {
			if (output_file[0] == '-' && output_file[1] == '\0') {
				no_fork = 1;
				rrdtool = stdout;
				send_log(LOG_INFO,"Output: stdout");
			} else { 
				rrdtool = fopen(output_file, "a");
				send_log(LOG_INFO,"Output: file");
			}
			if (!rrdtool) {
				send_log(LOG_ERR, "Failed opening %s: %s\n", output_file, strerror(errno));
				return 3;
			}
		} else if (mode == PIPE_MODE) {
			chdir(workdir);
			rrdtool = popen(output_pipe, "w");
			send_log(LOG_INFO,"Update method: pipe");
			if (!rrdtool) {
				send_log(LOG_ERR, "Failed opening pipe %s: %s\n", output_pipe, strerror(errno));
				return 3;
			}
			make_update = &print_update;
		}
		
		/* implicit flushing on each line */
		setlinebuf(rrdtool);
	}

	if (chdir(workdir)) {
		send_log(LOG_ERR, "Cannot chdir to %s: %s\n", workdir, strerror(errno));
		return 2;
	}

#ifdef HAVE_FORK
	if (!no_fork && fork())
		return 0;

	if (!no_fork) {
		/*
		fclose(stdin);
		fclose(stdout);
		fclose(stderr);
		*/
		close(0);
		close(1);
		if(debugoutput!=OUTPUT_STDERR) {
			close(2);
		}
		open("/dev/null",O_RDWR);
		dup2(0,1);
		if(debugoutput!=OUTPUT_STDERR) {
			dup2(0,2);
		}
		/* open("/tmp/tomojlog.txt",O_WRONLY|O_CREAT); */

	}
#endif /* HAVE_FORK */

	write_pidfile();

	/* being raised every "step" */
	action.sa_handler = &do_nothing;
	action.sa_flags = 0;
	sigaction(SIGALRM, &action, NULL);

	/* user requests, et. al. */
	action.sa_handler = &do_action;
	action.sa_flags = 0;
	sigaction(SIGINT, &action, NULL);
	sigaction(SIGQUIT, &action, NULL);
	sigaction(SIGTERM, &action, NULL);
	sigaction(SIGHUP, &action, NULL);
	sigaction(SIGUSR1, &action, NULL);
	sigaction(SIGUSR2, &action, NULL);

	/* deep trouble */
	action.sa_handler = &do_abort;
	action.sa_flags = 0;
	sigaction(SIGSEGV, &action, NULL);
	sigaction(SIGFPE, &action, NULL);
	sigaction(SIGBUS, &action, NULL);
	
	period.it_interval.tv_sec  = period.it_value.tv_sec  = step;
	period.it_interval.tv_usec = period.it_value.tv_usec = 0;
	setitimer(ITIMER_REAL, &period, NULL);

	loop = 1;
	while (loop) {
		if (action_request) {
			switch (action_request) {
				case SIGINT:
				case SIGQUIT:
				case SIGTERM:
					send_log(LOG_NOTICE,"Signal received. Preparing to exit...");
					loop=0;
					break;
					
				case SIGHUP:
				case SIGUSR1:
				case SIGUSR2:
				default:
					fflush(stdout);
					break;
			}
			
			action_request = 0;

		} else {
			perform_tests();
			print_results();
			clear_counters();
		}

		if(loop) pause();
	}
	/* Freeing the resources */
	release_pidfile();
	send_log(LOG_NOTICE,"Exitting.");
}
