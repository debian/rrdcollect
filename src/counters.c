/* counters.c - preparing space to store counters. */
/*
 *  RRDcollect  --  Round-Robin Database Collecting Daemon.
 *  Copyright (C) 2002, 2003  Dawid Kuroczko <qnex@knm.org.pl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "rrdcollect.h"

struct rrd_t **rrd = NULL;
int rrd_count = 0;

struct test_t **test = NULL;
int test_count = 0;

struct counter_t *add_rrd_counter(const char *rrdfile, const char *name)
{
	int i;

	for (i = 0; i < rrd_count; i++) {
		if (!strcmp(rrd[i]->rrdfile, rrdfile)) {
			int n;
			size_t len;
			for (n = 0; n < rrd[i]->count; n++) {
				if (!strcmp(rrd[i]->counter[n]->name, name)) {
					send_log(LOG_ERR, "duplicate target: `%s:%s'\n",
							rrdfile, name);
					exit(1);
				}
			}
			n = rrd[i]->count++;
			
			rrd[i]->counter = realloc(rrd[i]->counter,
					rrd[i]->count * sizeof(struct counter_t *));
			
			rrd[i]->counter[n] = malloc(sizeof(struct counter_t));
			rrd[i]->counter[n]->name = strdup(name);
			rrd[i]->counter[n]->value = NULL;
			
			len = strlen(rrd[i]->rrdtemplate);
			len += strlen(name) + 2;
			rrd[i]->rrdtemplate = realloc(rrd[i]->rrdtemplate, len);
			strcat(rrd[i]->rrdtemplate, ":");
			strcat(rrd[i]->rrdtemplate, name);

			return rrd[i]->counter[n];
		}
	}

	i = rrd_count++;
	rrd = realloc(rrd, rrd_count * (sizeof(struct rrd_t *)));
	rrd[i] = malloc(sizeof(struct rrd_t));

	rrd[i]->rrdfile = strdup(rrdfile);
	rrd[i]->count = 1;

	rrd[i]->counter = malloc(sizeof(struct counter_t *));
	rrd[i]->counter[0] = malloc(sizeof(struct counter_t));
	rrd[i]->counter[0]->name = strdup(name);
	rrd[i]->counter[0]->value = NULL;

	rrd[i]->rrdtemplate = strdup(name);

	return rrd[i]->counter[0];
}

struct test_t *add_test_uri(const struct uri_t *uri)
{
	int i = test_count++;
	test = realloc(test, test_count * (sizeof(struct test_t *)));
	test[i] = malloc(sizeof(struct test_t));

	test[i]->uri = malloc(sizeof(struct uri_t));
	memcpy(test[i]->uri, uri, sizeof(struct uri_t));

	test[i]->count = 0;
	test[i]->match = NULL;

	return test[i];
}

void clear_counters(void)
{
	int i;
	for (i = 0; i < rrd_count; i++) {
		int n;

		for (n = 0; n <rrd[i]->count; n++) {
			if (rrd[i]->counter[n]->value) {
				free(rrd[i]->counter[n]->value);
				rrd[i]->counter[n]->value = NULL; /* redundant? */
			}
		}
	}
}

void perform_tests()
{
	int i;
	for (i = 0; i < test_count; i++) {
		FILE *input;
		int n;
		int line = 0;

#ifdef ENABLE_EXEC
		/* Horribly ugly and shameful code... */
		if (*(test[i]->uri->domain) == 'e') { /* exec */
			input = popen(test[i]->uri->path, "r");
		} else { /* file */
#endif /* ENABLE_EXEC */
			input = fopen(test[i]->uri->path, "r");
#ifdef ENABLE_EXEC
		}
#endif /* ENABLE_EXEC */

		if (!input) {
			send_log(LOG_ERR, "Cannot open file %s: %s\n", test[i]->uri->path, strerror(errno));
			errno = 0;
			continue; /* FIXME: more intelligent error handling! */
		}
		
		while (fgets(buf, sizeof(buf), input)) {
			line++;
			
			for (n = 0; n < test[i]->count; n++) {
				const struct match_t *match = test[i]->match[n];
				struct counter_t **tmpcnt;
				int j;

				if (match->line && match->line != line)
					continue;

				tmpcnt=malloc(match->count*sizeof(struct counter_t *));
				for(j=0;j<match->count;j++) {
					tmpcnt[j]=malloc(sizeof(struct counter_t));
					tmpcnt[j]->name=match->counter[j]->name;
					tmpcnt[j]->value=NULL;
				}

				/* FIXME: use the return value! */
				switch (match->type) {
					case MATCH_PATTERN:
					case MATCH_PATSUBST: /* for now */
						if(scan(buf, match->pattern, tmpcnt)==0) {
							for(j=0;j<match->count;j++) {
								if(tmpcnt[j]->value) {
									if(match->counter[j]->value) free(match->counter[j]->value);
									match->counter[j]->value=strdup(tmpcnt[j]->value);
								}
							}
						}
						break;
#ifdef HAVE_LIBPCRE
					case MATCH_REGEX:
						regscan(buf, match);
						break;
#endif /* HAVE_LIBPCRE */
				}

				for(j=0;j<match->count;j++) {
					if(tmpcnt[j]->value) free(tmpcnt[j]->value);
					free(tmpcnt[j]);
				}
				free(tmpcnt);
			}
		}

#ifdef ENABLE_EXEC
		if (*(test[i]->uri->domain) == 'e') { /* exec */
			pclose(input);
		} else {
#endif /* ENABLE_EXEC */
			fclose(input);
#ifdef ENABLE_EXEC
		}
#endif /* ENABLE_EXEC */
	}
}

void print_results(void)
{
	const char *embed[] = { "update", "-t", NULL, NULL, NULL, NULL };
	int i;
	for (i = 0; i < rrd_count; i++) {
		int n;
		char *bp = buf;
		
		bp += sprintf(bp, "%ld", time(NULL));
		
		for (n = 0; n <rrd[i]->count; n++) {
			const struct counter_t *counter = rrd[i]->counter[n];
			char *p;

			/* FIXME: checking wheter bp > buf+sizeof(buf) */
			*(bp++) = ':';
			if ((p = counter->value)) {
				while (*p) {
					*(bp++) = *(p++);
				}
			} else {
				*(bp++) = 'U';
			}
		}
		*bp = '\0';

		embed[2] = rrd[i]->rrdtemplate;
		embed[3] = rrd[i]->rrdfile;
		embed[4] = buf;

		make_update(5, (char **)embed);
	}
}
