.TH RRDCOLLECT 8 "RRDcollect !VERSION!" "2 September 2002" "RRDcollect"
.SH NAME
RRDcollect  --  Round-Robin Database Collecting Daemon.
.SH SYNOPSIS
\fBrrdcollect\fR [\fIoption\fR]...

.SH DESCRIPTION
The \fBrrdcollect\fR daemon is responsible for fetching statistical
data from various soures (listed in \fBrrdcollect.conf\fR),
typically files under \fB/proc/\fR directory.  It periodically
reads them and searches for patterns from configuration file.
Values are then stored inside \fBrrdtool\fR's \fI.rrd\fR files.

.SH OPTIONS
RRDcollect follows the usual GNU command line syntax, with long
options starting with two dashes (`-').

.SS General options
.IP "\fB\-c\fR \fIfile\fR" 4
.PD 0
.IP "\fB\-\-config=\fIfile\fR" 4
.PD
Specifies which \fIfile\fR should be read instead of default config file.

.IP "\fB\-n\fR" 4
.PD 0
.IP "\fB\-\-no\-fork\fR" 4
.PD
Program will not fork.  Very useful when testing or debugging.

.IP "\fB\-v\fB" 4
.PD 0
.IP "\fB\-\-verbose\fR" 4
.PD
Increase verbosity.  This option also implies \fB\-\-no\-fork\fR, since
you probably don't want RRDcollect in daemon mode trashing your terminal.

.SS RRD update options

.IP "\fB\-o\fR \fIfile\fR" 4
.PD 0
.IP "\fB\-\-output=\fIfile\fR" 4
Specifies \fIfile\fR to which \fIrrdtool\fR(1)-style commands should
be written.  If \fIfile\fR you specify is `\fB-\fR' (hyphen), then commans
are written to standard output (this case implies \fB\-\-no\-fork\fR).

.IP "\fB\-p\fR" 4
.PD 0
.IP "\fB\-\-pipe\fR" 4
.PD 0
.IP "\fB\-\-pipe=\fIprogram\fR" 4
.PD
When this option is used, RRDcollect will open pipe to \fIrrdtool\fR(1)
program and send it update commands.  If you don't give \fIprogram\fR,
the compiled-in default is assumed (either `\fBrrdtool - >/dev/null\fR'
or full path to \fIrrdtool\fR(1) binary).  The \fIprogram\fR option
can be specified as a full path or just a command name, in the latter
case the program should reside on \fBPATH\fR.

.PP
If no RRD update option is given, RRDcollect uses internal librrd routines
(the preferred mode of operation).  If RRDcollect is not compiled
with librrd, then it will fall back to pipe-mode.

.SS Help and information
.IP "\fB\-V\fR" 4
.PD 0
.IP "\fB\-\-version\fR" 4
.PD
Display RRDcollect's version, compile-time options and license.

.IP "\fB\-h\fR" 4
.PD 0
.IP "\fB\-\-help\fR" 4
.PD
Show summary of options.

.IP "\fB\-\-usage\fR"
.PD
Show short usage information.

.SH FILES
.B !rrdcollect_conf!

.SH "SEE ALSO"
.IR rrdcollect.conf (5),
.IR rrdtool (1),
.IR pcre (3)

.SH BUGS
Hopefully not many...

.SH AUTHOR
.nf
Dawid Kuroczko <qnex@knm.org.pl>
Artur R. Czechowski <arturcz@hell.pl>
.fi
.\" vim:filetype=nroff
