.TH RRDCOLLECT.CONF 5 "RRDcollect !VERSION!" "2 September 2002" "RRDcollect"
.SH NAME
rrdcollect.conf  --  RRDcollect configuration file.
.SH SYNOPSIS
.B !rrdcollect_conf!

.SH DESCRIPTION
The \fIrrdcollect.conf\fR file contains information where to look
for data and to which database file put it.

.SS Variables

.IP
.nf
# Configuration values:
step = 60
directory = /var/local/rrd
loglevel = LOG_NOTICE
.fi
.LP

.SS Patterns
.IP
.nf
# System statistics:
file:///proc/stat
"cpu %d %d %d %d"    stat.rrd:user,nice,system,idle
"processes %u"       stat.rrd:processes
"swap %u %u"         stat.rrd:swap_in,swap_out
.fi
.LP

.IP
.nf
# System load: 1, 5 and 15 min. average
file:///proc/loadavg
"%f %f %f"           avg1.rrd:load,avg5.rrd:load,avg15.rrd:load
.fi
.LP

.IP
.nf
# Memory usage:
file:///proc/meminfo
"Mem:  %*d %d %d %d %d %d"    memory.rrd:used,free,shared,buffers,cached
"Swap: %*d %d %*d"            memory.rrd:swap_used
.fi
.LP

.IP
.nf
# S.M.A.R.T. HDD temperature:
file:///proc/ide/hda/smart_values
7:"%*04x %*04x %02x%*02x"       temperature.rrd:hda
.fi
.LP

.SS Regular expressions
.IP
.nf
# Using regular expressions:
file:///proc/stat
/cpu  (\\d+) (\\d+) (\\d+) (\\d+)/    stat.rrd:user,nice,system,idle
.fi
.LP

Please look into examples/ directory for working examples.

.SH FILES
.B !rrdcollect_conf!

.SH "SEE ALSO"
.IR rrdcollect (8),
.IR rrdtool (1),
.IR pcre (3)

.SH AUTHOR
.nf
Dawid Kuroczko <qnex@knm.org.pl>
Artur R. Czechowski <arturcz@hell.pl>
.fi
.\" vim:filetype=nroff
