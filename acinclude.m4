# A function which will pretty-print `--with' help options.
AC_DEFUN([QX_ARG_WITH], [
	# TODO: Add posibility to specify where to look for libs/includes
	AC_ARG_WITH([lib$1],
		AC_HELP_STRING([--with-lib$1], [use $2 (autodetect)]),
		ac_cv_use_$1=$withval, ac_cv_use_$1=yes)
	if test "$ac_cv_use_$1" = yes; then
		AC_CHECK_HEADERS([$1.h $1/$1.h], ac_cv_use_$1=yes; break, ac_cv_use_$1=no)
	fi
	if test "$ac_cv_use_$1" = yes; then
		AC_CHECK_LIB([$1], [$3])
	fi
])

# Searching for additional programs.
AC_DEFUN([QX_ARG_WITH_PROG], [
	# TODO: Add posibility to give exact path for programs
	AH_TEMPLATE([$1], [Define here where to find $2 program.  Can be simply command name, if it is on $PATH.])
	AC_ARG_WITH([$2],
		AC_HELP_STRING([--with-$2=$1], [location of $2 program]),
		prog_$1=$withval, prog_$1=yes)
	if test "$prog_$1" = yes; then
		AC_CHECK_PROG([$1], [$2], [$2], [no])
	elif test "$prog_$1" != no; then
		AC_MSG_CHECKING([for $2])
		if test -x $prog_$1; then
			AC_MSG_RESULT([$prog_$1])
			$1=$prog_$1
		else
			AC_MSG_ERROR([The $2 program: $prog_$1 you specified does not exist or is not executable!])
		fi
	fi
	if test "$$1" = no; then
		AC_MSG_WARN([forcing $1 to have \"$2\" value.])
		$1=$2
	fi
	AC_DEFINE_UNQUOTED([$1], ["$$1 - >/dev/null"])
])

# A function which will pretty-print `--enable' help options.
# Unlike --with, we don't check for existence of given feature.
AC_DEFUN([QX_ARG_ENABLE], [
	AH_TEMPLATE([$2], [Compile in $3.])
	AC_ARG_ENABLE([$1],
		AC_HELP_STRING([--enable-$1], [enable $3 (disable)]),
		ac_cv_enable_$1=$enableval, ac_cv_enable_$1=no)
	AC_MSG_CHECKING([whether to enable $3])
	if test "$ac_cv_enable_$1" = yes; then
		AC_MSG_RESULT([yes])
		AC_DEFINE([$2])
	else
		AC_MSG_RESULT([no])
	fi
])

# Hmm, seems a bit ugly, but will do for now.
AC_DEFUN([QX_ANONYMOUS_UNION], [
	AH_TEMPLATE(HAVE_ANONYMOUS_UNION, [Define if compiler can handle anonymous unions inside structs.])
	AC_MSG_CHECKING([whether anonymous unions work])
	AC_CACHE_VAL(ac_cv_anonymous_union,[
		AC_TRY_COMPILE([
		    struct {
			union {
			    int bar;
			    char *baz;
			};
		    } foo;
		],[
		    foo.bar = 42;
		    foo.baz = "42";
		],ac_cv_anonymous_union=yes,ac_cv_anonymous_union=no)
	])
	if test "$ac_cv_anonymous_union" = yes; then
		AC_MSG_RESULT([yes])
		AC_DEFINE(HAVE_ANONYMOUS_UNION)
	else
		AC_MSG_RESULT([no])
	fi
])

# getopt_long, system, -lrrd or our own...
AC_DEFUN([QX_FUNC_GETOPT_LONG], [
	AC_CHECK_FUNCS([getopt_long])
	use_our_own_getop_long=no
	if test "$ac_cv_func_getopt_long" = no; then
		AC_CHECK_LIB([rrd], [getopt_long])
		if test "$ac_cv_lib_rrd_getopt_long" = no; then
			use_our_own_getop_long=yes
		fi
	fi
	AM_CONDITIONAL([USE_OUR_GETOPT], [test "$use_our_own_getop_long" = yes])
])

# Follow the white m4. ;-)
AC_DEFUN([QX_FUNC_SPOON], [
	# You think it's m4 you're reading now?
	AC_MSG_CHECKING([for spoon])
	# AC_CACHE_VAL(ac_cv_func_spoon, ac_cv_func_spoon=no)
	AC_ARG_WITH([spoon],
		AC_HELP_STRING([--with-spoon], [there is no fork(2) (without)]),
		ac_cv_func_spoon=$withval)
	if test "$ac_cv_func_spoon" = yes; then
		AC_MSG_RESULT([yes])
		AC_MSG_WARN([Do not try and bend the system.])
		sleep 1
		AC_MSG_WARN([Instead only try to realize the truth.])
		sleep 1
		AC_MSG_WARN([There is no spoon(2).])
		ac_cv_func_spoon=no
		sleep 2
	else
		AC_MSG_RESULT([there is no spoon])
	fi
])

